package me.dwliu.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring Boot 启动类
 *
 * @author liudw
 * @date 2018/7/7 13:12
 **/
@SpringBootApplication
@RestController
public class DemoApplication {
    public static void main(String[] args) {
        //spring boot 启动类
        SpringApplication.run(DemoApplication.class, args);

    }

    @GetMapping(value = "/hello")
    public String hello() {
        return "Hello Spring Security";
    }
}
